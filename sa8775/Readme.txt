The below listed Kernel patches can be applied cleanly on top of Redhat Kernel ER1.2 tag.

ER1.2 tag URL: https://git.codelinaro.org/clo/la/kernel/msm/-/tree/ER1.2
Top commit SHA of ER1.2 : 27a81a076fd1d3e8045a402cb29dfbdc505b2ad6


Bringup Patches:
================

0001-config-Enable-missing-configs-for-Lemans-SoC.patch          
0002-arm64-dts-qcom-Add-lemans-SoC-DT-support.patch              
0003-ARM-dts-msm-Add-pinctrl-node-for-TLMM-on-LeMans-SoC.patch   
0004-pinctrl-qcom-Add-support-for-Lemans-SoC-TLMM.patch          
0005-WIP-Serial-console-enable.patch                             
0006-regulator-qcom-rpmh-Add-pmm8650-regulators.patch            
0007-soc-qcom-rpmhpd-add-lemans-rpmh-power-domains.patch         
0008-dts-add-regulators-for-lemans.patch                         
0009-rpmhpd-support-for-lemans.patch                             
0010-arm64-dts-qcom-include-qcom-rpmpd.h-in-lemans-dtsi.patch    
0011-dt-bindings-clock-add-support-for-clock-ids-for-lema.patch  
0012-clk-qcom-add-lemans-GCC-driver.patch                        
0013-clk-qcom-clk-alpha-pll-Don-t-reconfigure-running-Tri.patch  
0014-clk-qcom-Add-LUCID_EVO-PLL-type-for-SDX65.patch             
0015-clk-qcom-clk-alpha-pll-Increase-PLL-lock-detect-poll.patch  
0016-clk-qcom-clk-alpha-pll-fix-clk_trion_pll_configure-d.patch  
0017-clk-qcom-clk-alpha-pll-limit-exported-symbols-to-GPL.patch  
0018-clk-qcom-clk-alpha-pll-add-Lucid-EVO-PLL-configurati.patch  
0019-clk-qcom-rpmh-Add-support-for-RPMH-clocks-for-lemans.patch  
0020-dt-bindings-icc-add-endpoint-IDs-for-interconnects-f.patch  
0021-interconnect-qcom-Add-lemans-interconnect-provider-d.patch  
0022-arm64-dts-qcom-add-interconnect-devices-for-Lemans.patch    
0023-cpufreq-blocklist-Lemans-in-cpufreq-dt-platdev.patch        
0024-hack-clk-qcom-clk-hack-to-enable-serial-console.patch       
0025-dt-bindings-phy-qcom-qmp-add-lemans-ufs-compatible.patch    
0026-phy-qcom-qmp-add-lemans-UFS-PHY.patch                       
0027-arm64-dts-qcom-enable-apps-and-pcie-smmu.patch              
0028-iommu-arm-smmu-qcom-add-support-for-qcom-sa8755p-v50.patch  
0029-arm64-dts-lemans-correct-vreg-s-nodes-for-e-regulato.patch  
0030-arm64-boot-dts-qcom-enable-primary-UFS-for-lemans.patch     
0031-hack-arm64-dts-remove-min-max-ranges-for-smps4-and-s.patch  
0032-arm64-dts-qcom-add-xo_baord_clk-frequency.patch             
0033-regulator-qcom-rpmh-correct-resource-name-for-ldo8-a.patch
0034-arm64-dts-qcom-add-qupv3-nodes-for-lemans-platform.patch
0035-clk-qcom-add-emac-gdsc-support-for-lemans.patch
0036-clk-qcom-correction-of-clk_alpha_pll_regs-for-gpll0.patch
0037-arm64-dts-qcom-add-qup-opp-table.patch
0038-revert-removed-hacks-for-enabling-serial-console.patch
0039-dt-bindings-phy-qcom-usb-snps-femto-v2-Add-lemans-co.patch
0040-dt-bindings-phy-qcom-qmp-Add-compatible-for-lemans-U.patch
0041-phy-qcom-qmp-Add-lemans-USB3-UNI-phy.patch
0042-arm64-dts-qcom-added-primary-usb-node-for-lemans.patch
0043-rh-configs-enable-required-configs-for-pmic-peripher.patch
0044-lemans-pmic-peripheral-support.patch
0045-rh-configs-enable-all-mmcc-related-configs.patch
0046-clk-qcom-clk-alpha-pll-add-Rivian-EVO-PLL-configurat.patch
0047-clk-qcom-alpha-pll-add-support-for-power-off-mode-fo.patch
0048-clk-qcom-camcc-Add-camera-clock-controller-for-Leman.patch
0049-clk-qcom-videocc-Add-video-clock-controller-for-LEMA.patch
0050-clk-qcom-gpucc-Add-graphics-clock-controller-for-Lem.patch
0051-clk-qcom-dispcc-Add-display-clock-controllers-for-le.patch
0052-clk-qcom-add-missing-reset-maps-for-clock-controller.patch
0053-arm64-dts-qcom-add-mmcc-nodes.patch
0054-dt-bindings-Add-IPCC-client-ID-for-GPDSP0-GPDSP1.patch
0055-arm64-dts-qcom-add-mproc-driver-support-for-Lemans.patch
0056-hack-clk-qcom-hack-to-enable-all-mmcc-s-clocks.patch
0057-arm64-dts-qcom-enable-kgsl-smmu.patch
0058-soc-qcom-smem-Update-max-processor-count.patch
0059-rh-configs-enable-smem-and-hwspinlock-configs.patch
0060-clk-qcom-lemans-differentiate-names-of-mdss-gdsc-s.patch
0061-clk-qcom-remove-alway_on-flag-for-gx-gdsc.patch
0062-arm64-dts-qcom-lemans-remove-power-domains-from-gpuc.patch
0063-hack-arm64-dts-qcom-lemans-disable-regulator-allow-s.patch
0064-pinctrl-qcom-spmi-pmic-Add-pm8775-gpio-compatible-st.patch
0065-lemans-tz-smcc-bypass-enable-gcc-and-cpufreq_hw-inte.patch

Note: Please apply all the patches using git am.




