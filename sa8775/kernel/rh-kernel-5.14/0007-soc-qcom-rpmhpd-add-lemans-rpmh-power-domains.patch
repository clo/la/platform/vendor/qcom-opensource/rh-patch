From 053cc85b82b5963940399628f86ea19cfb503e57 Mon Sep 17 00:00:00 2001
From: Veera Vegivada <quic_vvegivad@quicinc.com>
Date: Fri, 16 Sep 2022 17:11:00 +0530
Subject: [PATCH 07/65] soc: qcom: rpmhpd: add lemans rpmh power-domains

Add power domains found in QTI Lemans SoC.

Signed-off-by: Veera Vegivada <quic_vvegivad@quicinc.com>
Signed-off-by: Patthan, Baji <quic_bpatthan@quicinc.com>
Change-Id: I9fba880febf37e79c70227a87feaf44610b1fa01
---
 drivers/regulator/qcom-rpmh-regulator.c |  6 ++---
 drivers/soc/qcom/rpmhpd.c               | 36 ++++++++++++++++++++++++-
 include/dt-bindings/power/qcom-rpmpd.h  | 19 +++++++++++++
 3 files changed, 57 insertions(+), 4 deletions(-)

diff --git a/drivers/regulator/qcom-rpmh-regulator.c b/drivers/regulator/qcom-rpmh-regulator.c
index 48d5579d3209..474c0a933ccf 100644
--- a/drivers/regulator/qcom-rpmh-regulator.c
+++ b/drivers/regulator/qcom-rpmh-regulator.c
@@ -1219,7 +1219,7 @@ static const struct rpmh_vreg_init_data pmr735a_vreg_data[] = {
 	{}
 };
 
-static const struct rpmh_vreg_init_data pmm8650_vreg_data[] = {
+static const struct rpmh_vreg_init_data pmm8775_vreg_data[] = {
 	RPMH_VREG("smps1",  "smp%s1",  &pmic5_ftsmps510,  "vdd-s1"),
 	RPMH_VREG("smps2",  "smp%s2",  &pmic5_ftsmps510,  "vdd-s2"),
 	RPMH_VREG("smps3",  "smp%s3",  &pmic5_ftsmps510,  "vdd-s3"),
@@ -1364,8 +1364,8 @@ static const struct of_device_id __maybe_unused rpmh_regulator_match_table[] = {
 		.data = pmr735a_vreg_data,
 	},
 	{
-		.compatible = "qcom,pmm8650-rpmh-regulators",
-		.data = pmm8650_vreg_data,
+		.compatible = "qcom,pmm8775-rpmh-regulators",
+		.data = pmm8775_vreg_data,
 	},
 	{}
 };
diff --git a/drivers/soc/qcom/rpmhpd.c b/drivers/soc/qcom/rpmhpd.c
index 092f6ab09acf..af82ace81069 100644
--- a/drivers/soc/qcom/rpmhpd.c
+++ b/drivers/soc/qcom/rpmhpd.c
@@ -185,6 +185,16 @@ static struct rpmhpd nsp = {
 	.res_name = "nsp.lvl",
 };
 
+static struct rpmhpd nsp0 = {
+	.pd = { .name = "nsp0", },
+	.res_name = "nsp0.lvl",
+};
+
+static struct rpmhpd nsp1 = {
+	.pd = { .name = "nsp1", },
+	.res_name = "nsp1.lvl",
+};
+
 static struct rpmhpd qphy = {
 	.pd = { .name = "qphy", },
 	.res_name = "qphy.lvl",
@@ -429,6 +439,29 @@ static const struct rpmhpd_desc sc8280xp_desc = {
 	.num_pds = ARRAY_SIZE(sc8280xp_rpmhpds),
 };
 
+/* Lemans RPMH powerdomains */
+static struct rpmhpd *lemans_rpmhpds[] = {
+	[LEMANS_CX] = &cx,
+	[LEMANS_CX_AO] = &cx_ao,
+	[LEMANS_EBI] = &ebi,
+	[LEMANS_GFX] = &gfx,
+	[LEMANS_LCX] = &lcx,
+	[LEMANS_LMX] = &lmx,
+	[LEMANS_MMCX] = &mmcx,
+	[LEMANS_MMCX_AO] = &mmcx_ao,
+	[LEMANS_MXC] = &mxc,
+	[LEMANS_MXC_AO] = &mxc_ao,
+	[LEMANS_MX] = &mx,
+	[LEMANS_MX_AO] = &mx_ao,
+	[LEMANS_NSP0] = &nsp0,
+	[LEMANS_NSP1] = &nsp1,
+};
+
+static const struct rpmhpd_desc lemans_desc = {
+	.rpmhpds = lemans_rpmhpds,
+	.num_pds = ARRAY_SIZE(lemans_rpmhpds),
+};
+
 static const struct of_device_id rpmhpd_match_table[] = {
 	{ .compatible = "qcom,sa8540p-rpmhpd", .data = &sa8540p_desc },
 	{ .compatible = "qcom,sc7180-rpmhpd", .data = &sc7180_desc },
@@ -443,6 +476,7 @@ static const struct of_device_id rpmhpd_match_table[] = {
 	{ .compatible = "qcom,sm8250-rpmhpd", .data = &sm8250_desc },
 	{ .compatible = "qcom,sm8350-rpmhpd", .data = &sm8350_desc },
 	{ .compatible = "qcom,sm8450-rpmhpd", .data = &sm8450_desc },
+	{ .compatible = "qcom,lemans-rpmhpd", .data = &lemans_desc },
 	{ }
 };
 MODULE_DEVICE_TABLE(of, rpmhpd_match_table);
@@ -724,4 +758,4 @@ static int __init rpmhpd_init(void)
 core_initcall(rpmhpd_init);
 
 MODULE_DESCRIPTION("Qualcomm Technologies, Inc. RPMh Power Domain Driver");
-MODULE_LICENSE("GPL v2");
+
diff --git a/include/dt-bindings/power/qcom-rpmpd.h b/include/dt-bindings/power/qcom-rpmpd.h
index 6cce5b7aa940..dc55e8922b70 100644
--- a/include/dt-bindings/power/qcom-rpmpd.h
+++ b/include/dt-bindings/power/qcom-rpmpd.h
@@ -283,6 +283,25 @@
 #define QCM2290_VDD_LPI_CX	6
 #define QCM2290_VDD_LPI_MX	7
 
+/* Lemans Power Domain Indexes */
+#define LEMANS_CX		0
+#define LEMANS_CX_AO		1
+#define LEMANS_DDR		2
+#define LEMANS_EBI		3
+#define LEMANS_GFX		4
+#define LEMANS_LCX		5
+#define LEMANS_LMX		6
+#define LEMANS_MMCX		7
+#define LEMANS_MMCX_AO		8
+#define LEMANS_MSS		9
+#define LEMANS_MX		10
+#define LEMANS_MX_AO		11
+#define LEMANS_MXC		12
+#define LEMANS_MXC_AO		13
+#define LEMANS_NSP0		14
+#define LEMANS_NSP1		15
+#define LEMANS_XO		16
+
 /* RPM SMD Power Domain performance levels */
 #define RPM_SMD_LEVEL_RETENTION       16
 #define RPM_SMD_LEVEL_RETENTION_PLUS  32
-- 
2.31.1

