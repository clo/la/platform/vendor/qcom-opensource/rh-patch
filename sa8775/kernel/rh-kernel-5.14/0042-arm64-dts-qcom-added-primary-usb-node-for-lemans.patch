From 6c66714069ec31b420f2a9c3e1777339d78f567b Mon Sep 17 00:00:00 2001
From: Shazad Hussain <quic_shazhuss@quicinc.com>
Date: Wed, 26 Oct 2022 18:23:15 +0530
Subject: [PATCH 42/65] arm64: dts: qcom: added primary usb node for lemans

primary usb nodes are being introduced for lemans with hs/ss phys.

Signed-off-by: Shazad Hussain <quic_shazhuss@quicinc.com>
Change-Id: Ic6e70e00d17c32c6d1e6e997d93aded498e33640
---
 arch/arm64/boot/dts/qcom/lemans.dts  | 23 +++++++
 arch/arm64/boot/dts/qcom/lemans.dtsi | 89 +++++++++++++++++++++++++++-
 2 files changed, 111 insertions(+), 1 deletion(-)

diff --git a/arch/arm64/boot/dts/qcom/lemans.dts b/arch/arm64/boot/dts/qcom/lemans.dts
index 9604ca77d191..c8d5a7d31517 100644
--- a/arch/arm64/boot/dts/qcom/lemans.dts
+++ b/arch/arm64/boot/dts/qcom/lemans.dts
@@ -275,6 +275,29 @@ &ufs_mem_phy {
 	status = "okay";
 };
 
+&usb_0 {
+	status = "okay";
+};
+
+&usb_0_dwc3 {
+	dr_mode = "peripheral";
+};
+
+&usb_0_hsphy {
+	vdda-pll-supply = <&vreg_l7a>;
+	vdda18-supply = <&vreg_l6c>;
+	vdda33-supply = <&vreg_l9a>;
+
+	status = "okay";
+};
+
+&usb_0_qmpphy {
+	vdda-phy-supply = <&vreg_l1c>;
+	vdda-pll-supply = <&vreg_l7a>;
+
+	status = "okay";
+};
+
 &xo_board_clk {
 	clock-frequency = <38400000>;
 };
diff --git a/arch/arm64/boot/dts/qcom/lemans.dtsi b/arch/arm64/boot/dts/qcom/lemans.dtsi
index 5434ef11e04c..a7a00ceb6650 100644
--- a/arch/arm64/boot/dts/qcom/lemans.dtsi
+++ b/arch/arm64/boot/dts/qcom/lemans.dtsi
@@ -390,7 +390,7 @@ gcc: clock-controller@100000 {
 				 <0>,
 				 <0>,
 				 <0>,
-				 <0>,
+				 <&usb_0_ssphy>,
 				 <0>,
 				 <0>,
 				 <0>,
@@ -753,6 +753,93 @@ ufs_mem_phy_lanes: phy@1d87400 {
 		};
 	};
 
+	usb_0_hsphy: phy@88e4000 {
+		compatible = "qcom,lemans-usb-hs-phy",
+					"qcom,usb-snps-hs-5nm-phy";
+		reg = <0x088e4000 0x120>;
+		clocks = <&rpmhcc RPMH_CXO_CLK>;
+		clock-names = "ref";
+		resets = <&gcc GCC_USB2_PHY_PRIM_BCR>;
+
+		#phy-cells = <0>;
+
+		status = "disabled";
+	};
+
+	usb_0_qmpphy: phy-wrapper@88e8000 {
+		compatible = "qcom,lemans-qmp-usb3-uni-phy";
+		reg = <0x088e8000 0x1c8>;
+		#address-cells = <1>;
+		#size-cells = <1>;
+		ranges;
+
+		clocks = <&gcc GCC_USB3_PRIM_PHY_AUX_CLK>,
+				<&rpmhcc RPMH_CXO_CLK>,
+				<&gcc GCC_USB_CLKREF_EN>,
+				<&gcc GCC_USB3_PRIM_PHY_COM_AUX_CLK>;
+		clock-names = "aux", "ref_clk_src", "ref", "com_aux";
+
+		resets = <&gcc GCC_USB3_PHY_PRIM_BCR>,
+				<&gcc GCC_USB3PHY_PHY_PRIM_BCR>;
+		reset-names = "phy", "common";
+
+		power-domains = <&gcc USB30_PRIM_GDSC>;
+
+		status = "disabled";
+
+		usb_0_ssphy: phy@88e8e00 {
+			reg = <0x088e8e00 0x160>,
+				  <0x088E9000 0x1ec>,
+				  <0x088e8200 0x1f0>;
+			#phy-cells = <0>;
+			#clock-cells = <0>;
+			clocks = <&gcc GCC_USB3_PRIM_PHY_PIPE_CLK>;
+			clock-names = "pipe0";
+			clock-output-names = "usb0_phy_pipe_clk_src";
+		};
+	};
+
+	usb_0: usb@a6f8800 {
+		compatible = "qcom,lemans-dwc3", "qcom,dwc3";
+		reg = <0x0a6f8800 0x400>;
+		#address-cells = <1>;
+		#size-cells = <1>;
+		ranges;
+
+		clocks = <&gcc GCC_CFG_NOC_USB3_PRIM_AXI_CLK>,
+				<&gcc GCC_USB30_PRIM_MASTER_CLK>,
+				<&gcc GCC_AGGRE_USB3_PRIM_AXI_CLK>,
+				<&gcc GCC_USB30_PRIM_SLEEP_CLK>,
+				<&gcc GCC_USB30_PRIM_MOCK_UTMI_CLK>;
+		clock-names = "cfg_noc", "core", "iface", "sleep", "mock_utmi";
+
+		assigned-clocks = <&gcc GCC_USB30_PRIM_MOCK_UTMI_CLK>,
+					<&gcc GCC_USB30_PRIM_MASTER_CLK>;
+		assigned-clock-rates = <19200000>, <200000000>;
+
+		interrupts = <GIC_SPI 287 IRQ_TYPE_LEVEL_HIGH>;
+		interrupt-names = "pwr_event";
+
+		power-domains = <&gcc USB30_PRIM_GDSC>;
+
+		resets = <&gcc GCC_USB30_PRIM_BCR>;
+
+		interconnects = <&aggre1_noc MASTER_USB3_0 0 &mc_virt SLAVE_EBI1 0>,
+				<&gem_noc MASTER_APPSS_PROC 0 &config_noc SLAVE_USB3_0 0>;
+		interconnect-names = "usb-ddr", "apps-usb";
+
+		status = "disabled";
+
+		usb_0_dwc3: usb@a600000 {
+			compatible = "snps,dwc3";
+			reg = <0x0a600000 0xe000>;
+			interrupts = <GIC_SPI 292 IRQ_TYPE_LEVEL_HIGH>;
+			iommus = <&apps_smmu 0x080 0x0>;
+			phys = <&usb_0_hsphy>, <&usb_0_ssphy>;
+			phy-names = "usb2-phy", "usb3-phy";
+		};
+	};
+
 	tlmm: pinctrl@f000000 {
 		compatible = "qcom,lemans-pinctrl";
 		reg = <0xf000000 0x1000000>;
-- 
2.31.1

