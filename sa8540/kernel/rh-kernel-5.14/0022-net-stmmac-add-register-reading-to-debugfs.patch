From 63825a6914016865faa782f990d950bdd92e771d Mon Sep 17 00:00:00 2001
From: Ning Cai <quic_ncai@quicinc.com>
Date: Wed, 8 Jun 2022 12:33:10 -0700
Subject: [PATCH 22/30] net: stmmac: add register reading to debugfs

Enable reading mac register config values from debugfs.
Enable reading dma register config values from debugfs.

Change-Id: Ic7e81b78f1a8a55c280e916ba10b71d8067a3e0a
Signed-off-by: Ning Cai <quic_ncai@quicinc.com>
---
 drivers/net/ethernet/stmicro/stmmac/dwmac4.h      |  6 ++-
 drivers/net/ethernet/stmicro/stmmac/dwmac4_dma.c  | 45 ++++++++++++++++
 drivers/net/ethernet/stmicro/stmmac/stmmac_main.c | 65 +++++++++++++++++++++++
 3 files changed, 115 insertions(+), 1 deletion(-)

diff --git a/drivers/net/ethernet/stmicro/stmmac/dwmac4.h b/drivers/net/ethernet/stmicro/stmmac/dwmac4.h
index 17f7d30..e399eaf 100644
--- a/drivers/net/ethernet/stmicro/stmmac/dwmac4.h
+++ b/drivers/net/ethernet/stmicro/stmmac/dwmac4.h
@@ -494,8 +494,12 @@ enum power_event {
 			GMAC_CONFIG_JE)
 
 /* To dump the core regs excluding  the Address Registers */
+#if IS_ENABLED(CONFIG_DWMAC_QCOM_VER3)
+#define	GMAC_REG_NUM	146
+#define DMA_CH_REG_NUM	17
+#else
 #define	GMAC_REG_NUM	132
-
+#endif
 /*  MTL debug */
 #define MTL_DEBUG_TXSTSFSTS		BIT(5)
 #define MTL_DEBUG_TXFSTS		BIT(4)
diff --git a/drivers/net/ethernet/stmicro/stmmac/dwmac4_dma.c b/drivers/net/ethernet/stmicro/stmmac/dwmac4_dma.c
index 5be8e6a..fd5ae00 100644
--- a/drivers/net/ethernet/stmicro/stmmac/dwmac4_dma.c
+++ b/drivers/net/ethernet/stmicro/stmmac/dwmac4_dma.c
@@ -179,6 +179,44 @@ static void dwmac4_dma_init(void __iomem *ioaddr,
 static void _dwmac4_dump_dma_regs(void __iomem *ioaddr, u32 channel,
 				  u32 *reg_space)
 {
+#if IS_ENABLED(CONFIG_DWMAC_QCOM_VER3)
+	int i = channel * DMA_CH_REG_NUM * 2;
+
+	reg_space[i + 4] = DMA_CHAN_CONTROL(channel);
+	reg_space[i + 5] = readl(ioaddr + DMA_CHAN_CONTROL(channel));
+	reg_space[i + 6] = DMA_CHAN_TX_CONTROL(channel);
+	reg_space[i + 7] = readl(ioaddr + DMA_CHAN_TX_CONTROL(channel));
+	reg_space[i + 8] = DMA_CHAN_RX_CONTROL(channel);
+	reg_space[i + 9] = readl(ioaddr + DMA_CHAN_RX_CONTROL(channel));
+	reg_space[i + 10] = DMA_CHAN_TX_BASE_ADDR(channel);
+	reg_space[i + 11] = readl(ioaddr + DMA_CHAN_TX_BASE_ADDR(channel));
+	reg_space[i + 12] = DMA_CHAN_RX_BASE_ADDR(channel);
+	reg_space[i + 13] = readl(ioaddr + DMA_CHAN_RX_BASE_ADDR(channel));
+	reg_space[i + 14] = DMA_CHAN_TX_END_ADDR(channel);
+	reg_space[i + 15] = readl(ioaddr + DMA_CHAN_TX_END_ADDR(channel));
+	reg_space[i + 16] = DMA_CHAN_RX_END_ADDR(channel);
+	reg_space[i + 17] = readl(ioaddr + DMA_CHAN_RX_END_ADDR(channel));
+	reg_space[i + 18] = DMA_CHAN_TX_RING_LEN(channel);
+	reg_space[i + 19] = readl(ioaddr + DMA_CHAN_TX_RING_LEN(channel));
+	reg_space[i + 20] = DMA_CHAN_RX_RING_LEN(channel);
+	reg_space[i + 21] = readl(ioaddr + DMA_CHAN_RX_RING_LEN(channel));
+	reg_space[i + 22] = DMA_CHAN_INTR_ENA(channel);
+	reg_space[i + 23] = readl(ioaddr + DMA_CHAN_INTR_ENA(channel));
+	reg_space[i + 24] = DMA_CHAN_RX_WATCHDOG(channel);
+	reg_space[i + 25] = readl(ioaddr + DMA_CHAN_RX_WATCHDOG(channel));
+	reg_space[i + 26] = DMA_CHAN_SLOT_CTRL_STATUS(channel);
+	reg_space[i + 27] = readl(ioaddr + DMA_CHAN_SLOT_CTRL_STATUS(channel));
+	reg_space[i + 28] = DMA_CHAN_CUR_TX_DESC(channel);
+	reg_space[i + 29] = readl(ioaddr + DMA_CHAN_CUR_TX_DESC(channel));
+	reg_space[i + 30] = DMA_CHAN_CUR_RX_DESC(channel);
+	reg_space[i + 31] = readl(ioaddr + DMA_CHAN_CUR_RX_DESC(channel));
+	reg_space[i + 32] = DMA_CHAN_CUR_TX_BUF_ADDR(channel);
+	reg_space[i + 33] = readl(ioaddr + DMA_CHAN_CUR_TX_BUF_ADDR(channel));
+	reg_space[i + 34] = DMA_CHAN_CUR_RX_BUF_ADDR(channel);
+	reg_space[i + 35] = readl(ioaddr + DMA_CHAN_CUR_RX_BUF_ADDR(channel));
+	reg_space[i + 36] = DMA_CHAN_STATUS(channel);
+	reg_space[i + 37] = readl(ioaddr + DMA_CHAN_STATUS(channel));
+#else
 	reg_space[DMA_CHAN_CONTROL(channel) / 4] =
 		readl(ioaddr + DMA_CHAN_CONTROL(channel));
 	reg_space[DMA_CHAN_TX_CONTROL(channel) / 4] =
@@ -213,12 +251,19 @@ static void _dwmac4_dump_dma_regs(void __iomem *ioaddr, u32 channel,
 		readl(ioaddr + DMA_CHAN_CUR_RX_BUF_ADDR(channel));
 	reg_space[DMA_CHAN_STATUS(channel) / 4] =
 		readl(ioaddr + DMA_CHAN_STATUS(channel));
+#endif
 }
 
 static void dwmac4_dump_dma_regs(void __iomem *ioaddr, u32 *reg_space)
 {
 	int i;
 
+#if IS_ENABLED(CONFIG_DWMAC_QCOM_VER3)
+	reg_space[0] = DMA_BUS_MODE;
+	reg_space[1] = readl(ioaddr + DMA_BUS_MODE);
+	reg_space[2] = DMA_SYS_BUS_MODE;
+	reg_space[3] = readl(ioaddr + DMA_SYS_BUS_MODE);
+#endif
 	for (i = 0; i < DMA_CHANNEL_NB_MAX; i++)
 		_dwmac4_dump_dma_regs(ioaddr, i, reg_space);
 }
diff --git a/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c b/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c
index cfa4cb8..efd7841 100644
--- a/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c
+++ b/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c
@@ -53,6 +53,14 @@
 #define	STMMAC_ALIGN(x)		ALIGN(ALIGN(x, SMP_CACHE_BYTES), 16)
 #define	TSO_MAX_BUFF_SIZE	(SZ_16K - 1)
 
+#if IS_ENABLED(CONFIG_DWMAC_QCOM_VER3)
+#define GMAC4_REG_NUM	146
+/*DMA_CH_REG_NUM * 1(ch used) * 2(offset+val) + 4(bus_mode & sys_bus_mode)*/
+#define V3_DMA_BUF_LEN	38
+#else
+#define GMAC4_REG_NUM	132
+#endif
+
 /* Module parameters */
 #define TX_TIMEO	5000
 static int watchdog = TX_TIMEO;
@@ -6176,6 +6184,55 @@ static int stmmac_dma_cap_show(struct seq_file *seq, void *v)
 }
 DEFINE_SHOW_ATTRIBUTE(stmmac_dma_cap);
 
+static int stmmac_mac_reg_show(struct seq_file *seq, void *v)
+{
+	struct net_device *dev = seq->private;
+	struct stmmac_priv *priv = netdev_priv(dev);
+	u32 reg[GMAC4_REG_NUM] = {0};
+	int i;
+
+	if (!priv->hw) {
+		seq_puts(seq, "HW address is null\n");
+		return 0;
+	}
+
+	stmmac_dump_mac_regs(priv, priv->hw, reg);
+	seq_puts(seq, "==============================\n");
+	seq_puts(seq, "\tReg offset     Value\n");
+	for (i = 0; i < GMAC4_REG_NUM; i++)
+		seq_printf(seq, "\t0x%x:           0x%x\n", i * 4, reg[i]);
+
+	seq_puts(seq, "==============================\n");
+
+	return 0;
+}
+DEFINE_SHOW_ATTRIBUTE(stmmac_mac_reg);
+
+static int stmmac_dma_reg_show(struct seq_file *seq, void *v)
+{
+	struct net_device *dev = seq->private;
+	struct stmmac_priv *priv = netdev_priv(dev);
+	u32 buf[V3_DMA_BUF_LEN] = {0};
+	int i;
+
+	if (!priv->ioaddr) {
+		seq_puts(seq, "HW address is null\n");
+		return 0;
+	}
+
+	stmmac_dump_dma_regs(priv, priv->ioaddr, buf);
+	seq_puts(seq, "==============================\n");
+	seq_puts(seq, "\tReg offset     Value\n");
+	for (i = 0; i < V3_DMA_BUF_LEN; i += 2)
+		seq_printf(seq, "\t0x%x:           0x%x\n",
+			   buf[i], buf[i + 1]);
+
+	seq_puts(seq, "==============================\n");
+
+	return 0;
+}
+DEFINE_SHOW_ATTRIBUTE(stmmac_dma_reg);
+
 /* Use network device events to rename debugfs file entries.
  */
 static int stmmac_device_event(struct notifier_block *unused,
@@ -6221,6 +6278,14 @@ static void stmmac_init_fs(struct net_device *dev)
 	debugfs_create_file("dma_cap", 0444, priv->dbgfs_dir, dev,
 			    &stmmac_dma_cap_fops);
 
+	/* Entry to report MAC register config value */
+	debugfs_create_file("mac_reg", 0444, priv->dbgfs_dir, dev,
+			    &stmmac_mac_reg_fops);
+
+	/* Entry to report DMA register config value */
+	debugfs_create_file("dma_reg", 0444, priv->dbgfs_dir, dev,
+			    &stmmac_dma_reg_fops);
+
 	rtnl_unlock();
 }
 
-- 
2.7.4

