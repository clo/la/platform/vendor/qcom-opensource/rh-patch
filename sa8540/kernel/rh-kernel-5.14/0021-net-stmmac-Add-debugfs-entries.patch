From 8b81157b1aa55f528ef55bc9974503b516497b93 Mon Sep 17 00:00:00 2001
From: Suraj Jaiswal <jsuraj@codeaurora.org>
Date: Tue, 15 Dec 2020 18:09:00 +0530
Subject: [PATCH 21/30] net: stmmac: Add debugfs entries

Add RGMII dump and phy register dump in debugfs.
Add debugfs clean up on remove.

Change-Id: Ib6e2f015d4fe682d25f1f61c3d2d40a69d64e794
Signed-off-by: Suraj Jaiswal <jsuraj@codeaurora.org>
---
 .../ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c    | 179 ++++++++++++++++++++-
 .../ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h    |   2 +
 2 files changed, 180 insertions(+), 1 deletion(-)

diff --git a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c
index 1f2d2b1..c80f8b1 100644
--- a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c
+++ b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c
@@ -13,7 +13,7 @@
 #include <linux/of_mdio.h>
 #include <linux/slab.h>
 #include <linux/poll.h>
-
+#include <linux/debugfs.h>
 #include <linux/dma-iommu.h>
 #include <linux/iommu.h>
 
@@ -745,6 +745,179 @@ inline void *qcom_ethqos_get_priv(struct qcom_ethqos *ethqos)
 	return priv;
 }
 
+static ssize_t read_phy_reg_dump(struct file *file, char __user *user_buf,
+				 size_t count, loff_t *ppos)
+{
+	struct qcom_ethqos *ethqos = file->private_data;
+	unsigned int len = 0, buf_len = 2000;
+	char *buf;
+	ssize_t ret_cnt;
+	int phydata = 0;
+	int i = 0;
+
+	struct platform_device *pdev = ethqos->pdev;
+	struct net_device *dev = platform_get_drvdata(pdev);
+	struct stmmac_priv *priv = netdev_priv(dev);
+
+	if (!ethqos || !dev->phydev) {
+		ETHQOSERR("NULL Pointer\n");
+		return -EINVAL;
+	}
+
+	buf = kzalloc(buf_len, GFP_KERNEL);
+	if (!buf)
+		return -ENOMEM;
+
+	len += scnprintf(buf + len, buf_len - len,
+					 "\n************* PHY Reg dump *************\n");
+
+	for (i = 0; i < 32; i++) {
+		phydata = ethqos_mdio_read(priv, priv->plat->phy_addr, i);
+		len += scnprintf(buf + len, buf_len - len,
+					 "MII Register (%#x) = %#x\n",
+					 i, phydata);
+	}
+
+	if (len > buf_len) {
+		ETHQOSERR("(len > buf_len) buffer not sufficient\n");
+		len = buf_len;
+	}
+
+	ret_cnt = simple_read_from_buffer(user_buf, count, ppos, buf, len);
+	kfree(buf);
+	return ret_cnt;
+}
+
+static ssize_t read_rgmii_reg_dump(struct file *file,
+				   char __user *user_buf, size_t count,
+				   loff_t *ppos)
+{
+	struct qcom_ethqos *ethqos = file->private_data;
+	unsigned int len = 0, buf_len = 2000;
+	char *buf;
+	ssize_t ret_cnt;
+	int rgmii_data = 0;
+
+	if (!ethqos) {
+		ETHQOSERR("NULL Pointer\n");
+		return -EINVAL;
+	}
+
+	buf = kzalloc(buf_len, GFP_KERNEL);
+	if (!buf)
+		return -ENOMEM;
+
+	len += scnprintf(buf + len, buf_len - len,
+					 "\n************* RGMII Reg dump *************\n");
+	rgmii_data = rgmii_readl(ethqos, RGMII_IO_MACRO_CONFIG);
+	len += scnprintf(buf + len, buf_len - len,
+					 "RGMII_IO_MACRO_CONFIG Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, SDCC_HC_REG_DLL_CONFIG);
+	len += scnprintf(buf + len, buf_len - len,
+					 "SDCC_HC_REG_DLL_CONFIG Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, SDCC_HC_REG_DDR_CONFIG);
+	len += scnprintf(buf + len, buf_len - len,
+					 "SDCC_HC_REG_DDR_CONFIG Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, SDCC_HC_REG_DLL_CONFIG2);
+	len += scnprintf(buf + len, buf_len - len,
+					 "SDCC_HC_REG_DLL_CONFIG2 Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, SDC4_STATUS);
+	len += scnprintf(buf + len, buf_len - len,
+					 "SDC4_STATUS Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, SDCC_USR_CTL);
+	len += scnprintf(buf + len, buf_len - len,
+					 "SDCC_USR_CTL Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, RGMII_IO_MACRO_CONFIG2);
+	len += scnprintf(buf + len, buf_len - len,
+					 "RGMII_IO_MACRO_CONFIG2 Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, RGMII_IO_MACRO_DEBUG1);
+	len += scnprintf(buf + len, buf_len - len,
+					 "RGMII_IO_MACRO_DEBUG1 Register = %#x\n",
+					 rgmii_data);
+	rgmii_data = rgmii_readl(ethqos, EMAC_SYSTEM_LOW_POWER_DEBUG);
+	len += scnprintf(buf + len, buf_len - len,
+					 "EMAC_SYSTEM_LOW_POWER_DEBUG Register = %#x\n",
+					 rgmii_data);
+
+	if (len > buf_len) {
+		ETHQOSERR("(len > buf_len) buffer not sufficient\n");
+		len = buf_len;
+	}
+
+	ret_cnt = simple_read_from_buffer(user_buf, count, ppos, buf, len);
+	kfree(buf);
+	return ret_cnt;
+}
+
+static const struct file_operations fops_phy_reg_dump = {
+	.read = read_phy_reg_dump,
+	.open = simple_open,
+	.owner = THIS_MODULE,
+	.llseek = default_llseek,
+};
+
+static const struct file_operations fops_rgmii_reg_dump = {
+	.read = read_rgmii_reg_dump,
+	.open = simple_open,
+	.owner = THIS_MODULE,
+	.llseek = default_llseek,
+};
+
+static int ethqos_create_debugfs(struct qcom_ethqos *ethqos)
+{
+	static struct dentry *phy_reg_dump;
+	static struct dentry *rgmii_reg_dump;
+	struct net_device *ndev = dev_get_drvdata(&ethqos->pdev->dev);
+
+	if (!ethqos) {
+		ETHQOSERR("Null Param\n");
+		return -ENOMEM;
+	}
+
+	ethqos->debugfs_dir = debugfs_create_dir(ndev->name, NULL);
+
+	if (!ethqos->debugfs_dir || IS_ERR(ethqos->debugfs_dir)) {
+		ETHQOSERR("Can't create debugfs dir\n");
+		return -ENOMEM;
+	}
+
+	phy_reg_dump = debugfs_create_file("phy_reg_dump", 0400,
+					   ethqos->debugfs_dir, ethqos,
+					   &fops_phy_reg_dump);
+	if (!phy_reg_dump || IS_ERR(phy_reg_dump)) {
+		ETHQOSERR("Can't create phy_dump %p\n", phy_reg_dump);
+		goto fail;
+	}
+
+	rgmii_reg_dump = debugfs_create_file("rgmii_reg_dump", 0400,
+					     ethqos->debugfs_dir, ethqos,
+					     &fops_rgmii_reg_dump);
+	if (!rgmii_reg_dump || IS_ERR(rgmii_reg_dump)) {
+		ETHQOSERR("Can't create rgmii_dump %p\n", rgmii_reg_dump);
+		goto fail;
+	}
+	return 0;
+
+fail:
+	debugfs_remove_recursive(ethqos->debugfs_dir);
+	return -ENOMEM;
+}
+
+static void ethqos_cleanup_debugfs(struct qcom_ethqos *ethqos)
+{
+	debugfs_remove_recursive(ethqos->debugfs_dir);
+	ethqos->debugfs_dir = NULL;
+
+	ETHQOSDBG("debugfs Deleted Successfully");
+}
+
 static int qcom_ethqos_probe(struct platform_device *pdev)
 {
 	struct device_node *np = pdev->dev.of_node;
@@ -757,6 +930,7 @@ static int qcom_ethqos_probe(struct platform_device *pdev)
 	if (of_device_is_compatible(pdev->dev.of_node,
 				    "qcom,emac-smmu-embedded"))
 		return emac_emb_smmu_cb_probe(pdev, plat_dat);
+
 	ret = stmmac_get_platform_resources(pdev, &stmmac_res);
 	if (ret)
 		return ret;
@@ -857,6 +1031,8 @@ static int qcom_ethqos_probe(struct platform_device *pdev)
 	if (ret)
 		goto err_clk;
 
+	ethqos_create_debugfs(ethqos);
+
 	rgmii_dump(ethqos);
 	qcom_ethqos_read_iomacro_por_values(ethqos);
 
@@ -903,6 +1079,7 @@ static int qcom_ethqos_remove(struct platform_device *pdev)
 		free_irq(ethqos->phy_intr, ethqos);
 	}
 
+	ethqos_cleanup_debugfs(ethqos);
 	ethqos_free_gpios(ethqos);
 	emac_emb_smmu_exit();
 	ethqos_disable_regulators(ethqos);
diff --git a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h
index b4d038f..9892e3b 100644
--- a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h
+++ b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h
@@ -52,6 +52,8 @@ struct qcom_ethqos {
 	struct regulator *reg_rgmii;
 	struct regulator *reg_emac_phy;
 	struct regulator *reg_rgmii_io_pads;
+
+	struct dentry *debugfs_dir;
 };
 
 int ethqos_init_reqgulators(struct qcom_ethqos *ethqos);
-- 
2.7.4

