From fcb36c85aedf24de20e1d0f68cf4ec40b992e9ce Mon Sep 17 00:00:00 2001
From: Aravind Vijayakumar <quic_aprasann@quicinc.com>
Date: Thu, 8 Dec 2022 17:17:45 -0800
Subject: [PATCH 57/58] misc: kiumd: add support for fixed IOVA change.

Adding a new ioctl call to handle the fixed IOVA
request from userspace, depending upon the value of
the flag for iommu cookie. To map the dma buf at a
specified IOVA address, add the address in msi_iova
of iommu dma cookie and set the cookie type to
msi cookie.

Change-Id: Ie20f5aee195643b9bc213c05d63ccd11bd5c6ce3
Signed-off-by: Aravind Vijayakumar <quic_aprasann@quicinc.com>
---
 drivers/iommu/iommu.c     |  2 +-
 drivers/misc/kiumd.c      | 67 +++++++++++++++++++++++++++++++++++++++
 include/uapi/misc/kiumd.h | 11 +++++++
 3 files changed, 79 insertions(+), 1 deletion(-)

diff --git a/drivers/iommu/iommu.c b/drivers/iommu/iommu.c
index dab42fdd63e3..ec54d55178e1 100644
--- a/drivers/iommu/iommu.c
+++ b/drivers/iommu/iommu.c
@@ -2117,7 +2117,7 @@ struct iommu_domain *iommu_get_dma_domain(struct device *dev)
 {
 	return dev->iommu_group->default_domain;
 }
-
+EXPORT_SYMBOL_GPL(iommu_get_dma_domain);
 /*
  * IOMMU groups are really the natural working unit of the IOMMU, but
  * the IOMMU API works on domains and devices.  Bridge that gap by
diff --git a/drivers/misc/kiumd.c b/drivers/misc/kiumd.c
index 34ec75930b08..09da175d2a1a 100644
--- a/drivers/misc/kiumd.c
+++ b/drivers/misc/kiumd.c
@@ -18,6 +18,10 @@
 #include <linux/vfio.h>
 #include <linux/hashtable.h>
 #include <uapi/misc/kiumd.h>
+#include <linux/iommu.h>
+#include <linux/types.h>
+#include <linux/dma-iommu.h>
+#include <linux/iova.h>
 
 /*Global Data structures needed for buffer sharing */
 static DEFINE_MUTEX(g_kiumd_lock);
@@ -37,6 +41,30 @@ struct kiumd_dev {
 	int fd;
 };
 
+enum iommu_dma_cookie_type {
+       IOMMU_DMA_IOVA_COOKIE,
+       IOMMU_DMA_MSI_COOKIE,
+};
+
+
+struct kiumd_iommu_dma_cookie {
+	enum iommu_dma_cookie_type      type;
+	union {
+        struct {
+               struct iova_domain      iovad;
+               struct iova_fq __percpu *fq;    /* Flush queue */
+               atomic64_t              fq_flush_start_cnt;
+               atomic64_t              fq_flush_finish_cnt;
+               struct timer_list       fq_timer;
+               atomic_t                fq_timer_on;
+             };
+               dma_addr_t              msi_iova;
+         };
+         struct list_head                msi_page_list;
+         struct iommu_domain             *fq_domain;
+};
+
+
 int kiumd_dmabuf_vfio_map(struct kiumd_dev *ki_dev, char __user *arg)
 {
 	struct kiumd_user kiusr;
@@ -205,6 +233,42 @@ int kiumd_dmabuf_fd(struct kiumd_dev *ki_dev, char __user *arg)
 	return 0;
 }
 
+int kiumd_iova_ctrl(struct kiumd_dev *ki_dev, char __user *arg)
+{
+	struct kiumd_iova iovausr;
+	struct file *file;
+	struct vfio_device *vfio_dev;
+	struct iommu_domain *domain = NULL;
+	struct kiumd_iommu_dma_cookie *cookie = NULL;
+	int cookie_type;
+	dma_addr_t iova_usr;
+
+	if (copy_from_user(&iovausr, arg, sizeof(struct kiumd_iova)))
+		return -EFAULT;
+
+	if(iovausr.iova_flag == KGSL_SMMU_GLOBALPT_FIXED_ADDR_CLEAR)
+		cookie_type = 0;
+	else
+		cookie_type = 1;
+
+	if(iovausr.iova_flag == KGSL_SMMU_GLOBALPT_FIXED_ADDR_SET) {
+		cookie_type = 1;
+		iova_usr = iovausr.iova;
+	}
+
+	file = fget(iovausr.vfio_fd);
+	vfio_dev = (struct vfio_device *)file->private_data;
+	domain = iommu_get_dma_domain(vfio_dev->dev);
+	cookie = (struct kiumd_iommu_dma_cookie*)domain->iova_cookie;
+	if(!cookie)	{
+		printk(KERN_INFO "kiumd_iova_ctrl: cookie not found\n");
+		return -ENOMEM;
+	}
+	cookie->type = cookie_type;
+	cookie->msi_iova = iova_usr;
+	return 0;
+}
+
 static int kiumd_open(struct inode *inode, struct file *filp)
 {
 	pr_err("kiumd_open called\n");
@@ -231,6 +295,9 @@ static long kiumd_ioctl(struct file *file, unsigned int cmd,
 	case KIUMD_IMPORT_DMABUF:
 		err = kiumd_import_fd(ki_dev, argp);
 		break;
+	case KIUMD_IOVA_MAP_CTRL:
+		err = kiumd_iova_ctrl(ki_dev, argp);
+		break;
 	default:
 		err = -ENOTTY;
 		break;
diff --git a/include/uapi/misc/kiumd.h b/include/uapi/misc/kiumd.h
index 2ceda4e5b087..069d92008f57 100644
--- a/include/uapi/misc/kiumd.h
+++ b/include/uapi/misc/kiumd.h
@@ -11,7 +11,18 @@
 #define KIUMD_SMMU_UNMAP_BUF	_IOWR('R', 11, struct kiumd_user)
 #define KIUMD_EXPORT_DMABUF		_IOWR('R', 12, struct kiumd_user)
 #define KIUMD_IMPORT_DMABUF		_IOWR('R', 13, struct kiumd_user)
+#define KIUMD_IOVA_MAP_CTRL             _IOWR('R', 14, struct kiumd_user)
 
+enum kiumd_iova_addr_type {
+	KGSL_SMMU_GLOBALPT_FIXED_ADDR_SET,
+	KGSL_SMMU_GLOBALPT_FIXED_ADDR_CLEAR,
+};
+
+struct kiumd_iova {
+	int vfio_fd;
+	enum kiumd_iova_addr_type iova_flag;
+	int iova;
+};
 
 struct kiumd_user {
 	int vfio_fd;
-- 
2.38.1

