The below listed Kernel patches can be applied cleanly on top of Redhat Kernel ER1.2 tag.

ER1.2 tag URL: https://git.codelinaro.org/clo/la/kernel/msm/-/tree/ER1.2
Top commit SHA of ER1.2 : 27a81a076fd1d3e8045a402cb29dfbdc505b2ad6


Ethernet:
=========
0001-dt-binding-Add-compatible-strings-to-stmmac-driver.patch 
0002-Fix-for-STMAC-config-conflicts.patch                      
0003-aarch64-config-make-stmmac-in-build.patch                 
0004-dt-binding-define-new-GDSC-for-EMAC.patch
0005-gcc-add-emac-gdsc-for-sc8280sp.patch                      
0006-arm64-dts-qcom-Add-emac-config.patch                      
0007-net-ethernet-disable-some-ethernet-drivers.patch          
0008-net-stmmac-SDx55-EMAC-HW-configuration-changes.patch      
0009-net-stmmac-Add-support-for-smmu-enablement.patch           
0010-net-stmmac-Add-IO-Macro-changes-HW-V3.patch               
0011-net-stmmac-Add-support-for-Mac2Mac.patch                  
0012-net-stmmac-Making-sure-to-not-register-for-phylink-f.patch
0013-net-stmmac-support-88Q5072-mac-switch.patch                
0014-net-stmmac-Change-HW-address-for-DMA-and-MTL-for-VER.patch
0015-net-stmmac-fix-a-null-pointer-dereference.patch           
0016-net-stmmac-Fix-ethtool-get-reg-crash.patch                
0017-net-stmmac-Add-support-for-PHY-interrupts.patch            
0018-net-phy-add-88EA1512-to-Marvell-PHY-driver.patch          
0019-net-stmmac-add-support-for-marvel-phy.patch               
0020-net-stmmac-bring-up-emac0-instance.patch                  
0021-net-stmmac-Add-debugfs-entries.patch
0022-net-stmmac-add-register-reading-to-debugfs.patch                     
0023-net-stmmac-SDx55-add-pps-signal-and-interrupts-suppo.patch
0024-net-stmmac-Enable-PPS0-on-bootup.patch
0025-net-stmmac-Add-multi-queue-support.patch
0026-net-stmmac-Add-support-for-pps-alignment-feature.patch
0027-net-stmmac-Support-eth-type-based-TX-intr-moderation.patch
0028-net-stmmac-Adding-IO-macro-and-HSR-settings.patch
0029-net-stmmac-HSR-seq-programming-in-ethqos_configure_m.patch
0030-arm64-dts-qcom-Add-io-macro-settings-to-emac-nodes.patch

PCIe:
====
0031-hack-pci-qcom-Add-SC8280XP-PCIe-support.patch
0032-hack-phy-qcom-qmp-Add-SC8280XP-PCIE-phy.patch
0033-hack-arm64-dts-Add-PCIE-3A-node-for-SC8280xp.patch
0034-arm64-dts-Add-PCIE-2A-node-for-SC8280xp.patch
0035-arm64-dts-enable-pcie3a-and-32bit-perf-non-perf-mem-.patch
 
FastRPC:
=======
0036-fastrpc-Adding-downstream-fastrpc-support-from-msm-k.patch
0037-fastrpc-update-the-Kconfig-and-Makefile-for-fastrpc.patch
0038-fastrpc-changes-in-dtsi-for-fastrpc.patch
0039-fastrpc-Enabling-FastRPC-and-it-s-dependent-configur.patch
0040-fastrpc-disabling-the-session-check-for-2-PDs-suppor.patch
0041-fastrpc-increasing-the-memory-size-to-support-larger.patch
0042-remoteproc-support-for-SA8540-NSPs-in-the-remoteproc.patch
0043-soc-qcom-fix-compilation-error.patch

Note: patch No.0035 was removed from the repo list, as it is not required.
